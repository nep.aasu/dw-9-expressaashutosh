// make express application
// assign port to that application

import express, { json } from "express";
import bikeRouter from "./src/Router/bikeRouter.js";
import firstRouter from "./src/Router/firstRouter.js";

import secondRouter from "./src/Router/secondRouter.js";
import thirdRouter from "./src/Router/thirdRouter.js";
import traineeRouter from "./src/Router/traineeRouter.js";
import vehiclesRouter from "./src/Router/vehiclesRouter.js";
let expressApp = express();
expressApp.use(json()); // always place this code at top of the router
let port = 8000;
expressApp.listen(port, () => {
  console.log(`express app is listening at port ${port}`);
});

/* expressApp.use(
  (req, res, next) => {
    console.log("first application middleware");
    next();
  },
  (req, res, next) => {
    console.log("second application middleware");
    next();
  },
  (req, res, next) => {
    console.log("third application middleware");
    next(); // if not call next it will not got to route
  }
); */

expressApp.use("/", firstRouter);
expressApp.use("/bike", bikeRouter);
expressApp.use("/trainee", traineeRouter);
expressApp.use("/vehicles", vehiclesRouter);

expressApp.use(thirdRouter);
