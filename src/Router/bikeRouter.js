import { Router } from "express";

let bikeRouter = Router();
bikeRouter
  .route("/")
  .post(
    (req, res, next) => {
      console.log("middleware 1st");
      next(1);
    },
    (err, req, res, next) => {
      console.log("middleware 2nd");
      next("error");
    },
    (err, req, res, next) => {
      console.log("middleware 3rd");
      next("ram");
    },
    (err, req, res, next) => {
      console.log("middleware 4rd");
    }
  )
  .get(() => {
    console.log("get bike");
  })
  .patch(() => {
    console.log("patch bike");
  })
  .delete(() => {
    console.log("delete bike");
  });

export default bikeRouter;
